import re
import os
import multiprocessing

########################
INPUT_DIR = "inputs"
OUTPUT_DIR = "outputs"
DIC = "dic.txt"
MIN_LETTERS = 3
########################


def process_text(txt):
    return re.sub(r"[^\w]", '', txt).lower()


def process_dic(dic_file):
    dic = open(dic_file, 'r').read()
    dic = re.sub(r"[^\w]", '\n', dic).lower().splitlines()
    return dic


def process_input(infile):
    file = open(INPUT_DIR+'/'+infile, "r")
    dic = process_dic(DIC)
    # print(dic)
    raw_text = file.read()
    # print(raw_text)
    txt = process_text(raw_text)
    # print(txt)
    all_possible_words = set()
    for i in range(1, len(txt)):
        for j in range(0, len(txt)):
            all_possible_words.add(txt[j: j + i])

    filtered_words = sorted([word for word in all_possible_words if word in dic])
    # print(filtered_words)
    os.mkdir(OUTPUT_DIR) if not os.path.exists(OUTPUT_DIR) else ()
    output = open(OUTPUT_DIR+'/'+infile + ".out.txt", "w")
    [output.write(word + '\n') for word in filtered_words]


if __name__ == '__main__':
    pool = multiprocessing.Pool()
    pool.map(process_input, [infile for infile in os.listdir(INPUT_DIR)])
